const HOVER_INTERNAL_ID = 'internal_hover';

function mouseEnter(event) {
    const hoverTextBox = document.createElement('div');
    hoverTextBox.id = HOVER_INTERNAL_ID;
    hoverTextBox.innerText = this.title;
    hoverTextBox.className = 'hoverText';
    hoverTextBox.setAttribute('style', `position: absolute; left: ${event.target.offsetLeft + 10}px; top: ${event.target.offsetTop - 10}px`);
    event.target.appendChild(hoverTextBox);
    event.stopPropagation();
}

function mouseLeave(event) {
    for (element of event.target.children) {
        if (element.id === HOVER_INTERNAL_ID) {
            event.target.removeChild(element);
        }
    }
    event.stopPropagation();
}
   
function addHoverText() {
    let elements = document.getElementsByClassName('onHover');
    for(element of elements) {
        element.addEventListener('mouseenter', mouseEnter);
        element.addEventListener('mouseleave', mouseLeave);
    }
}

window.onload = addHoverText;